import Vue from 'vue'
import Http from './util/http'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'

Vue.use(Http, {
  baseURL: 'http://localhost',
  timeout: 3000
  // withCredentials: false
})

Vue.config.productionTip = false
Vue.use(ElementUI)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
